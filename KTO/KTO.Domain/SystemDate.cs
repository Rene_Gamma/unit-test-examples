﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTO.Domain
{
    public class SystemDate
    {
        public static DateTime? FixedTime { get; set; }

        public static DateTime Now
        {
            get { return FixedTime.HasValue ? FixedTime.Value : DateTime.Now; }
        }

        public static DateTime UtcNow
        {
            get { return FixedTime.HasValue ? FixedTime.Value.ToUniversalTime() : DateTime.UtcNow; }
        }
    }
}
