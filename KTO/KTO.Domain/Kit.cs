﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTO.Domain
{
    public class Kit
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Requestor { get; set; }

        public DateTime DateFirstSubmitted { get; set; }
    }
}
