﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTO.Domain
{
    public class CreateKitRequest
    {
        public string Name { get; set; }

        public string Requestor { get; set; }
    }
}
