﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KTO.Domain
{
    public class CreateKitManager
    {
        public IKitRepository KitRepository { get; private set; }

        public CreateKitManager(IKitRepository kitRepository)
        {
            if(kitRepository == null)
                throw new ArgumentNullException("kitRepository");
            KitRepository = kitRepository;
        }

        public Kit Handle(CreateKitRequest request)
        {
            if(request == null)
                throw new ArgumentNullException("request");
            var newKit = new Kit
                {
                    Name = request.Name,
                    Requestor = request.Requestor,
                    DateFirstSubmitted = SystemDate.UtcNow
                };
            return KitRepository.Save(newKit);
        }
    }
}
