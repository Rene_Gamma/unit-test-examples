﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace KTO.Domain.Tests
{
    [TestClass]
    public class CreateKitManagerTests
    {

        [TestMethod]
        public void Create_WithNullKitRepository_ThrowsArgumentNullExceptionForKitRepository()
        {
            // Arrange
            var fixture = new CreateKitManagerFixture()
                .WithNullKitRepository();
            // Act
            try
            {
                fixture.CreateSut();
                Assert.Fail();
            }
            catch (ArgumentNullException ane)
            {
                // Assert
                Assert.AreEqual("kitRepository", ane.ParamName);
            }
        }

        [TestMethod]
        public void Handle_WhenRequestIsNull_ThrowsArgumentNullException()
        {
            // Arrange
            var fixture = new CreateKitManagerFixture();
            // Act
            try
            {
                fixture.CreateSut().Handle(null);
                Assert.Fail();
            }
            catch (ArgumentNullException ane)
            {
                // Assert
                Assert.AreEqual("request", ane.ParamName);
            }
        }

        [TestMethod]
        public void Handle_WhenRequestIsNotNull_ReturnsNotNullKit()
        {
            // Arrange
            var fixture = new CreateKitManagerFixture();
            // Act
            var actual = fixture.CreateSut().Handle(new CreateKitRequest());
            // Assert
            Assert.IsNotNull(actual);
        }

        [TestMethod]
        public void Handle_WithRequestWithValues_MapsValuesToKitCorrectly()
        {
            // Arrange
            var fixture = new CreateKitManagerFixture()
                .WithRequestWithValues();
            // Act
            var actual = fixture.CreateSut().Handle(fixture.KitRequest);
            // Assert
            Assert.AreEqual(fixture.KitRequest.Name, actual.Name);
            Assert.AreEqual(fixture.KitRequest.Requestor, actual.Requestor);
        }

        [TestMethod]
        public void Handle_WithNotNullRequest_GeneratesAutomaticValuesOnKit()
        {
            // Arrange
            var fixture = new CreateKitManagerFixture()
                .WithTodayAs(new DateTime(2014, 4, 14));
            // Act
            var actual = fixture.CreateSut().Handle(fixture.KitRequest);
            // Assert
            Assert.AreEqual(fixture.Today.ToUniversalTime(), actual.DateFirstSubmitted);
        }

        

        [TestMethod]
        public void Handle_WithNotNullRequest_PersistsKit()
        {
            // Arrange
            var kitRepository = new KitRepositoryDouble();
            var fixture = new CreateKitManagerFixture()
                .WithKitRepository(kitRepository);
            // Act
            fixture.CreateSut().Handle(fixture.KitRequest);
            // Assert
            Assert.IsTrue(kitRepository.SaveWasExecuted);
        }

        [TestMethod]
        public void Handle_WithValidKitRepository_TheKitIdIsGenerated()
        {
            // Arrange
            var kitRepository = new KitRepositoryDouble();
            var fixture = new CreateKitManagerFixture()
                .WithKitRepository(kitRepository);
            // Act
            var actual = fixture.CreateSut().Handle(fixture.KitRequest);
            // Assert
            Assert.AreNotEqual(0, actual.Id);
        }
    }

    class CreateKitManagerFixture
    {
        private IKitRepository _kitRepository;
        public CreateKitRequest KitRequest { get; private set; }
        public DateTime Today { get; private set; }

        public CreateKitManagerFixture()
        {
            _kitRepository = new KitRepositoryDouble();
            KitRequest = new CreateKitRequest();
        }

        public CreateKitManagerFixture WithRequestWithValues()
        {
            KitRequest = new CreateKitRequest
            {
                Name = "Test Name",
                Requestor = "Rene"
            };
            return this;
        }

        public CreateKitManagerFixture WithTodayAs(DateTime newToday)
        {
            Today = newToday;
            SystemDate.FixedTime = Today;
            return this;
        }

        public CreateKitManagerFixture WithKitRepository(IKitRepository kitRepository)
        {
            _kitRepository = kitRepository;
            return this;
        }

        public CreateKitManagerFixture WithNullKitRepository()
        {
            return WithKitRepository(null);
        }

        public CreateKitManager CreateSut()
        {
            return new CreateKitManager(_kitRepository);
        }
    }

    class KitRepositoryDouble : IKitRepository
    {
        public bool SaveWasExecuted { get; private set; }

        public Kit Save(Kit kit)
        {
            SaveWasExecuted = true;
            kit.Id = 1;
            return kit;
        }
    }
}
